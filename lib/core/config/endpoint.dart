class Endpoint {
  static String _baseURL =
      'http://transinfo.dishub.pasuruankab.go.id/public/rollapi';
  static String login = '$_baseURL/auth/login';
  static String employeeInformation = '$_baseURL/employee/employee-information';
  static String logout = '$_baseURL/auth/logout';
  static String _basePhotoUrl =
      'http://maxon.zapto.org:8080/uploads/employee-photo/';
}
