import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapppayrollauth/core/models/user_model.dart';
import 'package:flutterapppayrollauth/core/services/auth_services.dart';
import 'package:flutterapppayrollauth/core/storage/local_model.dart';
import 'package:flutterapppayrollauth/core/utils/toast_utils.dart';
import 'package:flutterapppayrollauth/ui/widget/top_profile.dart';
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';

class CustomerVisit extends StatefulWidget {
  static String route = '/customer';

  _CustomerStatePage createState() => _CustomerStatePage();
}

class _CustomerStatePage extends State<CustomerVisit> {
  String _time;
  String _dateTime;
  String _timeStart = "-";
  MaterialColor _statsColor = Colors.grey;
  UserModel _userModel = UserModel();
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isCompleteLoad = false;
  LocalStorage storage = new LocalStorage(storageName);
  String _statsTap = "OUT";

  final SnackBar userStatusSnackBar = SnackBar(
    content: Text("Please wait get user data ..."),
    duration: Duration(days: 365),
  );

  @override
  void initState() {
    super.initState();
    _time = _formatDateTime(DateTime.now());
    _dateTime = _formatDate(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    fetchUserModel().then((value) {
      _userModel = value;
    });
    getCustomerVisit().then((value){
      print("hasil value ${value.status}");
      if(value.status == "IN"){
        _statsTap = value.status;
        _timeStart = value.startIn;
        _statsColor = Colors.green;
      }
    });
  }

  Future<UserModel> fetchUserModel() async {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scaffoldKey.currentState.showSnackBar(userStatusSnackBar);
    });
    UserModel _response;
    if (await storage.ready) {
      UserLocal local = UserLocal();
      local.fromJson(await storage.getItem(keyUserLocal));
      _response = await AuthServices.getUserInformation(local.token);
      _isCompleteLoad = true;
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _scaffoldKey.currentState.hideCurrentSnackBar();
      });
    }
    return _response;
  }

  Future<void> getStatusVisit() async {
    LocalCustomerVisit localCustomerVisit = await getCustomerVisit();
    if(localCustomerVisit.status != null){
      _timeStart = localCustomerVisit.startIn;
      _statsTap = localCustomerVisit.status;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Container(
            color: Colors.white,
            child: CustomScrollView(
              slivers: <Widget>[
                TopProfile(
                  photoPath: 'images/users.jpg',
                  nameEmployee: _userModel.name,
                  department: _userModel.department,
                  position: _userModel.position,
                  isBackButton: true,
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.only(top: 5, left: 10, right: 10, bottom: 10),
                  sliver: SliverList(
                    delegate: SliverChildListDelegate([
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              onPressed: () {
                                onTapIn();
                              },
                              color: Color(0xff00317C),
                              textColor: Colors.white,
                              child: Text("IN"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(10),
                          ),
                          Container(
                            width: 100,
                            height: 100,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              onPressed: () {
                                onTapOut();
                              },
                              color: Color(0xff00317C),
                              textColor: Colors.white,
                              child: Text("OUT"),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.drafts,
                                color: Colors.yellowAccent[400],
                                size: 30.0,
                              ),
                              Text(
                                'Note',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.lightBlueAccent,
                                  fontFamily: 'RobotoBold',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(10),
                              ),
                              Icon(
                                Icons.assignment,
                                color: Colors.yellowAccent[400],
                                size: 30.0,
                              ),
                              Text(
                                'History',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.lightBlueAccent,
                                  fontFamily: 'RobotoBold',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            _time,
                            style: TextStyle(
                              fontSize: 40.0,
                              color: Colors.blue[900],
                              fontFamily: 'RobotoBold',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            _dateTime,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                                color: Colors.lightBlueAccent),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                //waktu
                                Text(
                                  _timeStart,
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.blue[900],
                                    fontFamily: 'RobotoBold',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  'Start Time',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.lightBlueAccent,
                                    fontFamily: 'RobotoBold',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ]),
                          Padding(
                            padding: EdgeInsets.only(left: 20, right: 20),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                // onTap: () {message();},
                                child: ClipOval(
                                  child: Container(
                                    color: _statsColor,
                                    height: 30.0,
                                    width: 30,
                                  ),
                                ),
                              ),
                              Text(
                                _statsTap,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.lightBlueAccent,
                                  fontFamily: 'RobotoBold',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 20, right: 20),
                          ),
                          Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  '07:00:00 ',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.blue[900],
                                    fontFamily: 'RobotoBold',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  'Duration',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.lightBlueAccent,
                                    fontFamily: 'RobotoBold',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ])
                        ],
                      )
                    ]),
                  ),
                )
              ],
            )));
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('hh:mm:ss').format(dateTime);
  }

  String _formatDate(DateTime datetime) {
    return DateFormat('d MMMM y, E').format(datetime);
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    setState(() {
      _time = formattedDateTime;
    });
  }

  Future<LocalCustomerVisit> getCustomerVisit() async {
    LocalCustomerVisit custVisit = new LocalCustomerVisit();
    if(await storage.ready){
      var json = await storage.getItem(keyCustomerVisitLocal);
      print("hasil check json $json");
      if(json!=null){
        custVisit.fromJson(json);
      }
    }
    print("cust visit $custVisit");
    return custVisit;
  }

  Future<void> onTapIn() async {
    if (_isCompleteLoad) {
      LocalCustomerVisit custVisit = await getCustomerVisit();
      print("hasil status check ${custVisit.status}");
      if(custVisit.status == null){
        _statsTap = "IN";
        DateTime dateTime = DateTime.now();
        _timeStart =  DateFormat('hh:mm:ss').format(dateTime);
        _statsColor = Colors.green;
        LocalCustomerVisit writeCustomerLocal = LocalCustomerVisit(
          status: "IN",
          startIn: dateTime.toString()
        );
        print("hasil write ${writeCustomerLocal.status} : ${writeCustomerLocal.startIn}");
        await storage.setItem(keyCustomerVisitLocal, writeCustomerLocal.toJsonEncodeAble());
      }
      Navigator.pushNamed(
          context, "/userVerification");
    } else {
      ToastUtils.show("Please wait ...");
    }
  }

  Future<void> onTapOut() async{

  }
}
