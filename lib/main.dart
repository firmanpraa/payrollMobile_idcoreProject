import 'package:flutter/material.dart';
import 'package:flutterapppayrollauth/ui/attendance.dart';
import 'package:flutterapppayrollauth/ui/claimApply.dart';
import 'package:flutterapppayrollauth/ui/customerVisit.dart';
import 'package:flutterapppayrollauth/ui/homepage.dart';
import 'package:flutterapppayrollauth/ui/leaveApply.dart';
import 'package:flutterapppayrollauth/ui/loadingPage.dart';
import 'package:flutterapppayrollauth/ui/loginPage.dart';
import 'package:flutterapppayrollauth/ui/message.dart';
import 'package:flutterapppayrollauth/ui/overtimeApply.dart';
import 'package:flutterapppayrollauth/ui/profile.dart';
import 'package:flutterapppayrollauth/ui/setting.dart';
import 'package:flutterapppayrollauth/ui/userVerification.dart';

void main(List<String> args) {
  runApp(
    new MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (context) => LoadingPage(),
        '/login': (context) => LoginPage(),
        '/home': (context) => HomePage(),
        '/attendance': (context) => Attendance(),
        '/customer': (context) => CustomerVisit(),
        '/userVerification': (context) => UserVerification(),
        '/leave': (context) => LeaveApply(),
        '/overtime': (context) => OvertimeApply(),
        '/claim': (context) => ClaimApply(),
        '/message': (context) => Message(),
        '/setting': (context) => Setting(),
        '/profile': (context) => Profile()
      },
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Payroll Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
