import 'package:dio/dio.dart';
import 'package:flutterapppayrollauth/core/models/auth_model.dart';
import 'package:flutterapppayrollauth/core/config/endpoint.dart';
import 'package:flutterapppayrollauth/core/models/user_model.dart';
import 'package:flutterapppayrollauth/core/utils/toast_utils.dart';

class AuthServices {
  static Dio dio = new Dio();

  static Future<auth_model> login(Map loginData) async {
    var model;
    try {
      var response = await dio.post(Endpoint.login,
          data: FormData.fromMap(loginData),
          options: Options(headers: {"Accept": "application/json"}));
      model = auth_model.fromJson(response.data, response.statusCode);
    } catch (e) {
      print(e);
    }
    return model;
  }

  static Future<UserModel> getUserInformation(String token) async {
    var model;
    try {
      var response = await dio.get(Endpoint.employeeInformation,
          options: Options(headers: {
            "Accept": "application/json",
            "Authorization": "Bearer $token"
          }));
      model = UserModel.fromJson(response.data, response.statusCode);
    } catch (e) {
      print(e);
    }
    return model;
  }
}
